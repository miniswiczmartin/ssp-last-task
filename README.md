# PackageService

[![pipeline status](https://gitlab.com/miniswiczmartin/ssp-last-task/badges/main/pipeline.svg)](https://gitlab.com/miniswiczmartin/ssp-last-task/-/commits/main)

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install required packages.

- pip install django
- pip install django-widget-tweaks

## Run

1. Go to the folder with the PackageService in your terminal.
2. Use the [Django](https://www.djangoproject.com/download/) package you just installed.

python3 manage.py runserver
