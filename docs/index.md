[![pipeline status](https://gitlab.com/miniswiczmartin/ssp-last-task/badges/main/pipeline.svg)](https://gitlab.com/miniswiczmartin/ssp-last-task/-/commits/main)


# About this project - Packages

This project is about how to use  Django to create websites.

# Contains
- Websites with using of Bootstrap
- Database 
- Admin site
- And more :D

## Instalation

```bash
pip install django
pip install django-widget-tweaks
```

## Start

```bash
python3 manage.py runserver
```

## Report
[Pylint](https://miniswiczmartin.gitlab.io/ssp-last-task/pylint.html)