from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from .models import *
from .forms import *

# Create your views here.
def index(request):
    packages = Package.objects.exclude(package_status='delivered')
    return render(request, 'Packages/packages.html', {'packages': packages })

def pckgdetail(request, package_id):
    package = get_object_or_404(Package, pk = package_id)
    special_features = package.special_features.all()
    return render(request, 'Packages/pckgdetail.html', {'package': package, 'special_features': special_features })

def pckgdelivered(request, package_id):
    package = get_object_or_404(Package, pk=package_id)
    package.package_status = 'delivered'
    package.save()
    return redirect('packages')

def pckgedit(request, package_id):
    package = Package.objects.get(pk = package_id)
    p_form = PackageForm(initial={
        'weight': package.weight,
        'package_status': package.package_status,
        'address': package.address,
        'special_features': package.special_features.all(),
        'courier': package.courier
    })
    return render(request, 'Packages/pckgedit.html', {'p_form': p_form, 'package': package })

def updatepckg(request, package_id):
    package = Package.objects.get(pk = package_id)
    p_form = PackageForm(request.POST)
    if p_form.is_valid():
        package.weight = p_form.cleaned_data['weight']
        package.package_status = p_form.cleaned_data['package_status']
        package.address = p_form.cleaned_data['address']
        package.special_features.set(p_form.cleaned_data['special_features'])
        package.courier = p_form.cleaned_data['courier']

        package.save()
        return redirect('packages')
    
def pckgadd(request):
    p_form = PackageForm
    return render(request, 'Packages/pckgadd.html', {'p_form': p_form })

def insertpckg(request):
    p_form = PackageForm(request.POST)
    if p_form.is_valid():
        p_weight = p_form.cleaned_data['weight']
        p_package_status = p_form.cleaned_data['package_status']
        p_address = p_form.cleaned_data['address']
        p_special_features = p_form.cleaned_data['special_features']
        p_courier = p_form.cleaned_data['courier']

        package = Package(weight = p_weight,
                        package_status = p_package_status,
                        address = p_address,
                        courier = p_courier)
        package.save()
        package.special_features.set(p_special_features)
        return redirect('packages')

def couriers(request):    
    couriers = Courier.objects.all()
    packages = {}
    for crr in couriers:
        packages[crr] = Package.objects.filter(courier = crr.id)

    return render(request, 'Couriers/couriers.html', {'couriers': couriers, 'packages': packages })

def courierdetail(request, courier_id):
    courier = get_object_or_404(Courier, pk = courier_id)
    return render(request, 'Couriers/courierdetail.html', {'courier': courier})

def courieredit(request, courier_id):
    courier = Courier.objects.get(pk = courier_id)
    c_form = CourierForm(initial={
        'name': courier.name,
        'surname': courier.surname,
        'at_work': courier.at_work,
        'login': courier.login,
        'area': courier.area
    })
    return render(request, 'Couriers/courieredit.html', {'c_form': c_form, 'courier': courier })

def updatecourier(request, courier_id):
    courier = Courier.objects.get(pk = courier_id)
    c_form = CourierForm(request.POST)
    if c_form.is_valid():
        courier.name = c_form.cleaned_data['name']
        courier.surname = c_form.cleaned_data['surname']
        courier.at_work = c_form.cleaned_data['at_work']
        courier.login=c_form.cleaned_data['login']
        courier.area = c_form.cleaned_data['area']

        courier.save()
        return redirect('courierdetail', courier_id)
    
def courieradd(request):
    c_form = CourierForm
    return render(request, 'Couriers/courieradd.html', {'c_form': c_form})

def insertcourier(request):
    c_form = CourierForm(request.POST)
    if c_form.is_valid():
        c_name = c_form.cleaned_data['name']
        c_surname = c_form.cleaned_data['surname']
        c_at_work = c_form.cleaned_data['at_work']
        c_login = c_form.cleaned_data['login']
        c_area = c_form.cleaned_data['area']

        courier = Courier(name = c_name,
                        surname = c_surname,
                        at_work = c_at_work,
                        login = c_login,
                        area = c_area)
        courier.save()
        return redirect('couriers')
    
def sfs(request):
    sfs = SpecialFeature.objects.all()
    return render(request, 'SpecialFeatures/sfs.html', {'sfs': sfs})

def sfadd(request):
    sf_form = SFForm
    return render(request, 'SpecialFeatures/sfadd.html', {'sf_form': sf_form})

def insertsf(request):
    sf_form = SFForm(request.POST)
    if sf_form.is_valid():
        sf_name = sf_form.cleaned_data['name']

        sf = SpecialFeature(name = sf_name)
        sf.save()
        return redirect('sfs')

def sfdelete(request, sf_id):
    sf = get_object_or_404(SpecialFeature, pk = sf_id)
    sf.delete()
    return redirect('sfs')

def cities(request):
    cities = City.objects.all()
    return render(request, 'Cities/cities.html', {'cities': cities })

def citydetail(request, city_id):
    city = get_object_or_404(City, pk = city_id)
    addresses = Address.objects.filter(city=city_id)
    return render(request, 'Cities/citydetail.html', {'city': city, 'addresses': addresses})

def cityadd(request):
    ct_form = CityForm
    return render(request, 'Cities/cityadd.html', {'ct_form': ct_form })

def insertcity(request):
    ct_form = CityForm(request.POST)
    if ct_form.is_valid():
        ct_name = ct_form.cleaned_data['name']
        ct_area = ct_form.cleaned_data['area']
        
        city = City(name = ct_name,
                        area = ct_area)
        city.save()
        return redirect('cities')
    
def cityedit(request, city_id):
    city = City.objects.get(pk = city_id)
    ct_form = CityForm(initial={
        'name': city.name,
        'area': city.area,
    })
    return render(request, 'Cities/cityedit.html', {'ct_form': ct_form, 'city': city })

def updatecity(request, city_id):
    city = City.objects.get(pk = city_id)
    ct_form = CityForm(request.POST)
    if ct_form.is_valid():
        city.name = ct_form.cleaned_data['name']
        city.area = ct_form.cleaned_data['area']

        city.save()
        return redirect('cities')
    
def deletecity(request, city_id):
    city = get_object_or_404(City, pk=city_id)
    city.delete()
    return redirect('cities')

def areas(request):
    areas = Area.objects.all()
    return render(request, 'Areas/areas.html', {'areas': areas })

def areadetail(request, area_id):
    area = get_object_or_404(Area, pk=area_id)
    cities = City.objects.filter(area=area_id)
    return render(request, 'Areas/areadetail.html', {'area': area, 'cities': cities })

def areaadd(request):
    a_form = AreaForm
    return render(request, 'Areas/areaadd.html', {'a_form': a_form })

def insertarea(request):
    a_form = CityForm(request.POST)
    if a_form.is_valid():
        a_name = a_form.cleaned_data['name']
        
        area = Area(area_name = a_name)
        area.save()
        return redirect('areas')
    
def deletearea(request, area_id):
    area = get_object_or_404(Area, pk=area_id)
    area.delete()
    return redirect('areas')

def addresses(request):
    addresses = Address.objects.all()
    return render(request, 'Addresses/addresses.html', {'addresses': addresses })

def addressdetail(request, address_id):
    address = get_object_or_404(Address, pk=address_id)
    return render(request, 'Addresses/addressdetail.html', {'address': address })

def addressadd(request):
    ad_form = AddressForm
    return render(request, 'Addresses/addressadd.html', {'ad_form': ad_form })

def insertaddress(request):
    ad_form = AddressForm(request.POST)
    if ad_form.is_valid():
        ad_street = ad_form.cleaned_data['street']
        ad_zip_code = ad_form.cleaned_data['zip_code']
        ad_city = ad_form.cleaned_data['city']
        
        address = Address(street = ad_street, zip_code=ad_zip_code, city=ad_city)
        address.save()
        return redirect('addresses')
    
def addressedit(request, address_id):
    address = get_object_or_404(Address, pk=address_id)
    ad_form = AddressForm(initial={
        'street': address.street,
        'zip_code': address.zip_code,
        'city': address.city
    })
    return render(request, 'Addresses/addressedit.html', {'ad_form':ad_form, 'address':address})

def updateaddress(request, address_id):
    address = get_object_or_404(Address, pk=address_id)
    ad_form = AddressForm(request.POST)
    if ad_form.is_valid():
        address.street = ad_form.cleaned_data['street']
        address.zip_code = ad_form.cleaned_data['zip_code']
        address.city = ad_form.cleaned_data['city']
        
        address.save()
        return redirect('addresses')
    
def deleteaddress(request, address_id):
    address = get_object_or_404(Address, pk=address_id)
    address.delete()
    return redirect('addresses')



