from django.test import TestCase
from .models import SpecialFeature

# Create your tests here.
class MyTest(TestCase):
    def setUp(self):
        SpecialFeature.objects.create(name='IDK1')
        SpecialFeature.objects.create(name='IDK2')

    def test1(self):
        sf1 = SpecialFeature.objects.filter(name='IDK1').first()
        sf2 = SpecialFeature.objects.filter(name='IDK2').first()

        self.assertFalse(sf1.name == sf2.name)
        self.assertFalse(sf2.name == sf1.name)

    def test2(self):
        sf1 = SpecialFeature.objects.filter(name='IDK1').first()
        sf2 = SpecialFeature.objects.filter(name='IDK2').first()

        self.assertTrue(sf1.name == sf1.name)
        self.assertTrue(sf2.name == sf2.name)

    def test3(self):
        sf1 = SpecialFeature.objects.filter(name='IDK1').first()
        sf2 = SpecialFeature.objects.filter(name='IDK2').first()

        self.assertTrue(sf1.name == sf2.name)
        self.assertTrue(sf2.name == sf1.name)