from django import forms
from .models import *

class PackageForm(forms.ModelForm):
    class Meta():
        model = Package
        exclude = ['date_dispatched', 'date_imported']

class CourierForm(forms.ModelForm):
    class Meta():
        model = Courier
        exclude = ['date_hired']

class SFForm(forms.ModelForm):
    class Meta():
        model = SpecialFeature
        exclude = []

class CityForm(forms.ModelForm):
    class Meta():
        model = City
        exclude = []

class AreaForm(forms.ModelForm):
    class Meta():
        model = Area
        exclude = []

class AddressForm(forms.ModelForm):
    class Meta():
        model = Address
        exclude = []