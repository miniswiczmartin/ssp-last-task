from django.db import models

# Create your models here.

class SpecialFeature(models.Model):
    name = models.CharField(max_length=10)

    def __str__(self) -> str:
        return self.name

class Area(models.Model):
    area_name = models.CharField(max_length=10)
    
    def __str__(self) -> str:
        return self.area_name

class Courier(models.Model):
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    at_work = models.BooleanField(default=False)
    login = models.CharField(max_length=10)
    date_hired = models.DateTimeField('date_hired', auto_now_add=True)
    area = models.ForeignKey(Area, on_delete=models.SET_NULL, null=True)

    def __str__(self) -> str:
        return self.name + ' ' + self.surname

class City(models.Model):
    area = models.ForeignKey(Area, on_delete=models.CASCADE)
    name = models.CharField(max_length=10)

    def __str__(self) -> str:
        return self.name
    
class Address(models.Model):
    street = models.CharField(max_length=15)
    zip_code = models.IntegerField()
    city = models.ForeignKey(City, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.street

class Package(models.Model):
    PACKAGE_STATUS = [
        ('dispatched', 'Dispatched'),
        ('imported', 'Imported'),
        ('returned', 'Returned'),
        ('delivered', 'Delivered')
    ]
    weight = models.DecimalField(max_digits=10, decimal_places=2)
    date_imported = models.DateTimeField('date_imported', auto_now_add=True)
    date_dispatched = models.DateTimeField(null=True, blank=True)
    package_status = models.CharField(max_length=10, choices=PACKAGE_STATUS)
    address = models.ForeignKey(Address, on_delete=models.CASCADE)
    special_features = models.ManyToManyField(SpecialFeature)
    courier = models.ForeignKey(Courier, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self) -> str:
        return str(self.pk)


