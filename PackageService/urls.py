"""
URL configuration for PackageService project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from Packages import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name = 'packages'),
    path('pckgdetail/<int:package_id>', views.pckgdetail, name = 'pckgdetail'),
    path('pckgdelivered/<int:package_id>', views.pckgdelivered, name = 'pckgdelivered'),
    path('pckgedit/<int:package_id>', views.pckgedit, name = 'pckgedit'),
    path('pckgedit/<int:package_id>/updatepckg', views.updatepckg, name = 'updatepckg'),
    path('pckgadd', views.pckgadd, name = 'pckgadd'),
    path('pckgadd/insertpckg', views.insertpckg, name = 'insertpckg'),
    path('couriers', views.couriers, name = 'couriers'),
    path('courierdetail/<int:courier_id>', views.courierdetail, name='courierdetail'),
    path('courieredit/<int:courier_id>', views.courieredit, name = 'courieredit'),
    path('courieredit/<int:courier_id>/updatecourier', views.updatecourier, name = 'updatecourier'),
    path('courieradd', views.courieradd, name = 'courieradd'),
    path('courieradd/insertcourier', views.insertcourier, name = 'insertcourier'),
    path('sfs', views.sfs, name = 'sfs'),
    path('sfsadd', views.sfadd, name = 'sfadd'),
    path('sfsadd/insertsf', views.insertsf, name = 'insertsf'),
    path('sfdelete/<int:sf_id>', views.sfdelete, name = 'sfdelete'),
    path('cities', views.cities, name = 'cities'),
    path('citydetail/<int:city_id>', views.citydetail, name = 'citydetail'),
    path('cityadd', views.cityadd, name = 'cityadd'),
    path('cityadd/insertcity', views.insertcity, name = 'insertcity'),
    path('cityedit/<int:city_id>', views.cityedit, name = 'cityedit'),
    path('cityedit/<int:city_id>/updatecity', views.updatecity, name = 'updatecity'),
    path('deletecity/<int:city_id>', views.deletecity, name = 'deletecity'),
    path('areas', views.areas, name='areas'),
    path('areadetail/<int:area_id>', views.areadetail, name='areadetail'),
    path('deletearea/<int:area_id>', views.deletearea, name='deletearea'),
    path('areaadd', views.areaadd, name='areaadd'),
    path('areaadd/insertarea', views.insertarea, name='insertarea'),
    path('addresses', views.addresses, name='addresses'),
    path('addressdetail/<int:address_id>', views.addressdetail, name='addressdetail'),
    path('addressadd', views.addressadd, name='addressadd'),
    path('addressadd/insertaddress', views.insertaddress, name='insertaddress'),
    path('addressedit/<int:address_id>', views.addressedit, name='addressedit'),
    path('addressedit/<int:address_id>/updateaddress', views.updateaddress, name='updateaddress'),
    path('deleteaddress/<int:address_id>', views.deleteaddress, name='deleteaddress')
]
